{-# LANGUAGE OverloadedStrings #-}
module Services.User
    (
      isAdmin,
      findUserByToken,
      createUser
    ) where

import           Common
import           Control.Arrow
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import           Data.Maybe
import           Data.Semigroup
import qualified Data.Text                  as T
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import           Models.Role
import           Models.Token
import           Models.User

createUser :: HQ.Query (T.Text, T.Text) User
createUser =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
        \WITH new_user AS (\
        \  INSERT INTO pvalue.users(username, _password)\
        \  VALUES ($1, crypt($2, gen_salt('bf', 8)))\
        \  RETURNING *\
        \), new_role AS (\
        \  INSERT INTO pvalue.roles(user_id, role)\
        \  SELECT new_user.id, 'USER'::pvalue.role\
        \  FROM new_user\
        \  RETURNING *\
        \)\
        \SELECT new_user.id, new_user.username, new_user.created_at \
        \FROM new_user\
      \"
      encoder :: HE.Params (T.Text, T.Text)
      encoder =
        contramap fst (HE.value HE.text) <>
        contramap snd (HE.value HE.text)
      decoder :: HD.Result User
      decoder = HD.singleRow $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz

findUserByToken :: HQ.Query TokenId User
findUserByToken =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ SELECT \
      \   u.id, \
      \   u.username, \
      \   u.created_at \
      \ FROM \
      \   pvalue.users u \
      \ INNER JOIN \
      \   pvalue.tokens t \
      \ ON \
      \   t.user_id = u.id \
      \ WHERE \
      \   t.token=$1 \
      \"
      encoder :: HE.Params TokenId
      encoder = contramap unTokenId (HE.value HE.uuid)
      decoder :: HD.Result User
      decoder = HD.singleRow $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz

isAdmin :: HQ.Query TokenId Bool
isAdmin =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ SELECT \
      \   COUNT(*) > 0 \
      \ FROM \
      \   pvalue.roles r \
      \ INNER JOIN \
      \   pvalue.tokens t \
      \ ON \
      \   t.user_id = r.user_id \
      \ WHERE \
      \   t.token=$1 \
      \ AND \
      \   r.role='ADMIN'::pvalue.role \
      \"
      encoder :: HE.Params TokenId
      encoder = contramap unTokenId (HE.value HE.uuid)
      decoder :: HD.Result Bool
      decoder = HD.singleRow $ HD.value HD.bool
