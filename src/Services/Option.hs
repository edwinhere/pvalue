{-# LANGUAGE OverloadedStrings #-}
module Services.Option
    (
      Outstanding,
      upsertOption,
      getOutstanding,
      price,
      cost,
      cost',
      buyOption,
      sellOption
    ) where

import           Common
import           Control.Arrow
import           Control.Monad.Error.Class
import qualified Data.ByteString            as BS
import           Data.Default
import           Data.Functor.Contravariant
import           Data.Int
import qualified Data.Map                   as M
import qualified Data.Maybe
import           Data.Scientific
import qualified Data.Semigroup             as S
import           Debug.Trace
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import qualified Hasql.Session              as HS
import qualified Hasql.Transaction          as HT
import qualified Models.Contract            as MC
import qualified Models.Entry               as ME
import qualified Models.Outcome             as MO
import qualified Models.Trade               as MT

type Option = (MC.Contract, [MO.Outcome])
type Outstanding = [(OutcomeId, Scientific)]

upsertOption :: Option -> HS.Session Option
upsertOption option = HT.run transaction HT.Serializable HT.Write
  where
    transaction :: HT.Transaction Option
    transaction = do
      contract <- HT.query (fst option) MC.upsertContract
      let outcomes = (\o -> o { MO._otContractId = MC._ctId contract }) <$> snd option
      outcomes' <- mapM (`HT.query` MO.upsertOutcome) outcomes
      return (contract, outcomes')

buyOption :: UserId -> OutcomeId -> Scientific -> HS.Session TradeId
buyOption uid oid q = HT.run transaction HT.Serializable HT.Write
  where
    transaction :: HT.Transaction TradeId
    transaction = do
      outstanding <- HT.query oid getOutstanding
      balance <- HT.query uid ME.getBalance
      let
        newOutstanding :: Outstanding
        newOutstanding = M.toList $ M.adjust (+ q) oid (M.fromList outstanding)
        c :: Double
        c = cost' outstanding newOutstanding
      if balance - fromFloatDigits c >= 0
        then do
          let
            order :: MT.Trade
            order = def { MT._trUserId = uid, MT._trPrice = fromFloatDigits c, MT._trQuantity = q, MT._trOutcomeId = oid, MT._trBuySell = MT.BUY }
          trade <- HT.query order MT.createTrade
          let
            entry :: ME.Entry
            entry = def { ME._eyType = ME.WITHDRAWAL, ME._eyAmount = fromFloatDigits c, ME._eyUserId = uid, ME._eyTradeId = Just (MT._trId trade) }
          _ <- HT.query entry ME.createEntry
          return $ MT._trId trade
        else fail "Low balance"

sellOption :: UserId -> OutcomeId -> Scientific -> HS.Session TradeId
sellOption uid oid quantity = HT.run transaction HT.Serializable HT.Write
  where
    transaction :: HT.Transaction TradeId
    transaction = do
      outstanding <- HT.query oid getOutstanding
      holding <- HT.query (uid, oid) MT.getHoldings
      if holding - quantity >= 0
        then do
          let
            newOutstanding :: Outstanding
            newOutstanding = M.toList $ M.adjust (subtract quantity) oid (M.fromList outstanding)
            c :: Double
            c = cost' outstanding newOutstanding
            order :: MT.Trade
            order = def { MT._trUserId = uid, MT._trPrice = abs (fromFloatDigits c), MT._trQuantity = quantity, MT._trOutcomeId = oid, MT._trBuySell = MT.SELL }
          trade <- HT.query order MT.createTrade
          let
            entry :: ME.Entry
            entry = def { ME._eyType = ME.DEPOSIT, ME._eyAmount = abs (fromFloatDigits c), ME._eyUserId = uid, ME._eyTradeId = Just (MT._trId trade) }
          _ <- HT.query entry ME.createEntry
          return $ MT._trId trade
        else fail "Uncovered short selling is not allowed"

alpha :: Double
alpha = 0.05
toMap :: Outstanding -> M.Map OutcomeId Double
toMap o = M.map toRealFloat $ M.fromList o
q :: Outstanding -> [Double]
q o = M.elems (toMap o)
sumq :: Outstanding -> Double
sumq o = sum (q o)
b :: Outstanding -> Double
b o = alpha * sumq o
qByB :: Outstanding -> [Double]
qByB o = fmap (/ b o) (q o)
expQByB :: Outstanding -> [Double]
expQByB o = fmap exp (qByB o)
logSum :: Outstanding -> Double
logSum o = (log . sum) (expQByB o)

cost :: Outstanding -> Double
cost o = b o * logSum o

cost' :: Outstanding -> Outstanding -> Double
cost' o n = cost n - cost o

price :: OutcomeId -> Outstanding -> Double
price i o = alpha * logSum o + term
  where
    qi :: Double
    qi = toMap o M.! i
    term :: Double
    term = (sum1 - sum2) / (sumq o * sum (expQByB o))
    sum1 :: Double
    sum1 = sum $ fmap (sumTerm qi) (q o)
    sum2 :: Double
    sum2 = sum $ fmap (\x -> sumTerm x x) (q o)
    sumTerm :: Double -> Double -> Double
    sumTerm y x = x * exp (y / b o)

getOutstanding :: HQ.Query OutcomeId Outstanding
getOutstanding =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ SELECT \
      \   o.id AS outcome, \
      \   SUM(t.quantity * CASE t.buy_sell \
      \                      WHEN 'BUY'::pvalue.buy_sell THEN 1 \
      \                      WHEN 'SELL'::pvalue.buy_sell THEN -1 \
      \                    END) AS outstanding \
      \ FROM pvalue.contracts c \
      \ JOIN pvalue.outcomes o ON o.contract_id = c.id \
      \ JOIN pvalue.trades t ON t.outcome_id = o.id \
      \ WHERE c.id IN (SELECT contract_id FROM pvalue.outcomes WHERE id = $1) \
      \ GROUP BY o.id \
      \"
      encoder :: HE.Params OutcomeId
      encoder = contramap unOutcomeId (HE.value HE.uuid)
      decoder :: HD.Result Outstanding
      decoder = HD.rowsList $
        (,) <$>
        (OutcomeId <$> HD.value HD.uuid) <*>
        HD.value HD.numeric
