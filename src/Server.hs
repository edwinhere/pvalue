{-# LANGUAGE CPP               #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}

module Server
  (main,app,appReader, settings, proxy, API)
  where

import           Common
import           Context
import           Control.Monad.Error.Class
import           Control.Monad.IO.Class
import           Control.Monad.Reader
import qualified Data.ByteString                  as BS
import           Data.Either
import           Data.Int
import           Data.Maybe
import           Data.String.Conv
import qualified Data.Text                        as T
import           Data.Text.Encoding
import           Data.Time.Clock
import           Data.UUID
import qualified Data.Vector                      as V
import           Debug.Trace
import           Error
import           GHC.Stack
import           Graphics.Captcha
import qualified Hasql.Connection                 as HC
import qualified Hasql.Pool                       as HP
import qualified Hasql.Session                    as HS
import qualified Models.Captcha                   as MCp
import qualified Models.Contract                  as MCt
import qualified Models.Token                     as MT
import qualified Models.User                      as MU
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Prelude                          ()
import           Prelude.Compat
import           Servant
import           Servant.Server.Experimental.Auth
import qualified Services.User                    as SU

#define ERROR (errorHandler __FILE__ __LINE__)

type API =
  "contracts" :> Get '[JSON] (V.Vector MCt.Contract) :<|>
  "captcha" :> Get '[JSON] MCp.Captcha :<|>
  "signup" :> AuthProtect "captcha" :> ReqBody '[JSON] (T.Text, T.Text) :> Post '[JSON] MU.User :<|>
  "login" :> AuthProtect "captcha" :> BasicAuth "login" MU.User :> Get '[JSON] String :<|>
  "admin" :> AuthProtect "admin" :> "users" :> QueryParam "before" UTCTime :> QueryParam "limit" Int64 :> Get '[JSON] (V.Vector MU.User) :<|>
  Raw

proxy :: Proxy API
proxy = Proxy

settings = HC.settings host port user password database
  where
    host = "localhost"
    port = 5432
    user = "edwin"
    password = "314159"
    database = "edwin"

server :: HP.Pool -> Server API
server pool =
  listContracts :<|>
  captcha :<|>
  signup :<|>
  login :<|>
  listUsers :<|>
  static
    where
      listContracts :: Handler (V.Vector MCt.Contract)
      listContracts = undefined
      captcha :: Handler MCp.Captcha
      captcha = do
        (c,i) <- liftIO makeCaptcha
        _1 <- liftIO $ HP.use pool (HS.query (toS c, i) MCp.createCaptcha)
        either ERROR return _1
      signup :: () -> (T.Text, T.Text) -> Handler MU.User
      signup _ (username,password) = do
          _1 <-
              liftIO $ HP.use pool (HS.query (username, password) SU.createUser)
          either ERROR return _1
      login :: () -> MU.User -> Handler String
      login _ user = do
          let _id = MU._urId user
          result <- liftIO $ HP.use pool (HS.query _id MT.upsertToken)
          either ERROR successHandler result
        where
          successHandler :: Maybe TokenId -> Handler String
          successHandler x = return $ maybe "" (show . unTokenId) x
      listUsers :: MU.User
                -> Maybe UTCTime
                -> Maybe Int64
                -> Handler (V.Vector MU.User)
      listUsers _ before limit = do
          let limit' = fromMaybe 10 limit
          _1 <-
              liftIO $ HP.use pool (HS.query (limit', before) MU.findNUsersBefore)
          either ERROR return _1
      static :: Server Raw
      static = serveDirectory "./static"

appReader :: Reader HP.Pool Application
appReader = do
    pool <- ask
    return $ serveWithContext proxy (context pool) (server pool)

app :: IO Application
app = do
  pool <- HP.acquire (20, realToFrac 3.154e+7, settings)
  return $ runReader appReader pool

main :: IO ()
main = app >>= run 8081
