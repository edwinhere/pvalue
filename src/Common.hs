{-# LANGUAGE Arrows                     #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Common
    (
      UserId(UserId),
      unUserId,
      TokenId(TokenId),
      unTokenId,
      RoleId(RoleId),
      unRoleId,
      CaptchaId(CaptchaId),
      unCaptchaId,
      ContractId(ContractId),
      unContractId,
      OutcomeId(OutcomeId),
      unOutcomeId,
      EntryId(EntryId),
      unEntryId,
      TradeId(TradeId),
      unTradeId,
      ByteString64(ByteString64),
      composeUpsert,
      composeUpsert',
    ) where

import           Control.Arrow
import           Data.Aeson             (FromJSON, ToJSON, Value (String),
                                         parseJSON, toJSON)
import           Data.Aeson.Types       (typeMismatch, withText)
import qualified Data.ByteString        as BS
import qualified Data.ByteString.Base64 as Base64
import           Data.Default
import           Data.Scientific
import qualified Data.Text              as T
import           Data.Text.Encoding
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.UUID              (UUID, fromString, nil)
import           GHC.Generics
import qualified Hasql.Query            as HQ

instance Default UUID where
  def = nil

instance Default UTCTime where
  def = UTCTime (ModifiedJulianDay 0) (secondsToDiffTime 0)

instance Default T.Text where
  def = T.empty

instance Default Scientific where
  def = 0

-- | Aeson serialisable bytestring. Uses base64 encoding.
newtype ByteString64 = ByteString64 { getByteString64 :: BS.ByteString }
    deriving (Eq, Show, Ord, Generic)

-- | Get base64 encode bytestring
getEncodedByteString64 :: ByteString64 -> BS.ByteString
getEncodedByteString64 = Base64.encode . getByteString64

instance ToJSON ByteString64 where
    toJSON = toJSON . decodeLatin1 . getEncodedByteString64

instance FromJSON ByteString64 where
    parseJSON = withText "ByteString" $
        pure . ByteString64 . Base64.decodeLenient . encodeUtf8


instance ToJSON UUID where
  toJSON = String . T.pack . show

instance FromJSON UUID where
  parseJSON json@(String t) =
    let uuidString = T.unpack t
    in case fromString uuidString of
         Just uuid -> pure uuid
         Nothing   -> typeMismatch "UUID" json
  parseJSON unknown = typeMismatch "UUID" unknown

newtype UserId = UserId { unUserId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype TokenId = TokenId { unTokenId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype RoleId = RoleId { unRoleId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype CaptchaId = CaptchaId { unCaptchaId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype ContractId = ContractId { unContractId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype OutcomeId = OutcomeId { unOutcomeId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype EntryId = EntryId { unEntryId :: UUID } deriving (Eq, Ord, Read, Show, Generic)
newtype TradeId = TradeId { unTradeId :: UUID } deriving (Eq, Ord, Read, Show, Generic)

instance Default UserId where
  def = UserId def

instance Default TokenId where
  def = TokenId def

instance Default RoleId where
  def = RoleId def

instance Default CaptchaId where
  def = CaptchaId def

instance Default ContractId where
  def = ContractId def

instance Default OutcomeId where
  def = OutcomeId def

instance Default EntryId where
  def = EntryId def

instance Default TradeId where
  def = TradeId def

instance ToJSON UserId where
  toJSON (UserId _id) = toJSON _id
instance FromJSON UserId where
  parseJSON x = fmap UserId (parseJSON x)

instance ToJSON TokenId where
  toJSON (TokenId _id) = toJSON _id
instance FromJSON TokenId where
  parseJSON x = fmap TokenId (parseJSON x)

instance ToJSON RoleId where
  toJSON (RoleId _id) = toJSON _id
instance FromJSON RoleId where
  parseJSON x = fmap RoleId (parseJSON x)

instance ToJSON CaptchaId where
  toJSON (CaptchaId _id) = toJSON _id
instance FromJSON CaptchaId where
  parseJSON x = fmap CaptchaId (parseJSON x)

instance ToJSON ContractId where
  toJSON (ContractId _id) = toJSON _id
instance FromJSON ContractId where
  parseJSON x = fmap ContractId (parseJSON x)

instance ToJSON OutcomeId where
  toJSON (OutcomeId _id) = toJSON _id
instance FromJSON OutcomeId where
  parseJSON x = fmap OutcomeId (parseJSON x)

instance ToJSON EntryId where
  toJSON (EntryId _id) = toJSON _id
instance FromJSON EntryId where
  parseJSON x = fmap EntryId (parseJSON x)

instance ToJSON TradeId where
  toJSON (TradeId _id) = toJSON _id
instance FromJSON TradeId where
  parseJSON x = fmap TradeId (parseJSON x)

-- Given an Update query,
-- which uses the @fmap (> 0) rowsAffected@ decoder
-- to detect, whether it had any effect,
-- and an Insert query,
-- produces a query which performs Upsert.
composeUpsert :: HQ.Query a Bool -> HQ.Query a () -> HQ.Query a ()
composeUpsert update insert =
  proc params -> do
    updated <- update -< params
    if updated
      then returnA -< ()
      else insert -< params

-- Given an Update query,
-- which uses the @fmap (> 0) rowsAffected@ decoder
-- to detect, whether it had any effect,
-- and an Insert query,
-- produces a query which performs Upsert.
composeUpsert' :: HQ.Query a Bool -> HQ.Query a a -> HQ.Query a a
composeUpsert' update insert =
  proc params -> do
    updated <- update -< params
    if updated
      then returnA -< params
      else insert -< params
