{-# LANGUAGE CPP               #-}
{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE TypeOperators     #-}
module Context (context) where

import           Common
import           Control.Monad                    (unless)
import           Control.Monad.IO.Class
import qualified Data.ByteString                  as BS
import           Data.Maybe
import           Data.String.Conv
import           Data.Text.Encoding
import           Data.UUID
import           Debug.Trace
import           Error
import qualified Hasql.Pool                       as HP
import qualified Hasql.Session                    as HS
import qualified Models.Captcha                   as C
import qualified Models.User                      as U
import           Network.Wai
import           Servant
import           Servant.API.Experimental.Auth
import           Servant.Server.Experimental.Auth
import qualified Services.User                    as US

#define ERROR (errorHandler __FILE__ __LINE__)

captchaCheck :: HP.Pool -> AuthHandler Request ()
captchaCheck pool = mkAuthHandler handler
    where
      handler :: Request -> Handler ()
      handler r =
        case lookup "Captcha" (requestHeaders r) of
          Nothing -> throwError $
            err401 {
                     errReasonPhrase = "Missing captcha"
                   }
          Just c -> do
            let splits = BS.split 32 c
                _solution = toS $ last splits
                _id = CaptchaId . fromMaybe nil $ fromASCIIBytes (head splits)
            result <- liftIO $ HP.use pool (HS.query (_id, _solution) C.checkCaptcha)
            result' <- either ERROR return result
            unless result' . throwError $
              err401 {
                  errReasonPhrase = "Incorrect captcha"
                }

userCheck :: HP.Pool -> AuthHandler Request U.User
userCheck pool = mkAuthHandler handler
  where
    handler :: Request -> Handler U.User
    handler request =
        case lookup "Authorization" (requestHeaders request) of
            Nothing ->
                throwError $
                err401
                { errReasonPhrase = "Missing authorization header"
                }
            Just v -> do
                let token' = last $ BS.split 32 v
                    uuid' = fromMaybe nil $ fromASCIIBytes token'
                    tokenId' = TokenId uuid'
                u <- liftIO $ HP.use pool (HS.query tokenId' US.findUserByToken)
                either ERROR return u

adminCheck :: HP.Pool -> AuthHandler Request U.User
adminCheck pool = mkAuthHandler handler
  where
    handler :: Request -> Handler U.User
    handler request =
        case lookup "Authorization" (requestHeaders request) of
            Nothing ->
                throwError $
                err401
                { errReasonPhrase = "Missing authorization header"
                }
            Just v -> do
                let token = last $ BS.split 32 v
                    uuid = fromMaybe nil $ fromASCIIBytes token
                    tokenId = TokenId uuid
                result <- liftIO $ HP.use pool (HS.query tokenId US.isAdmin)
                result' <- either ERROR return result
                if result'
                    then do
                        u <-
                            liftIO $
                            HP.use pool (HS.query tokenId US.findUserByToken)
                        either ERROR return u
                    else throwError $
                         err401
                         { errReasonPhrase = "You are not an administrator"
                         }

type instance AuthServerData (AuthProtect "admin") = U.User
type instance AuthServerData (AuthProtect "user") = U.User
type instance AuthServerData (AuthProtect "captcha") = ()

grantCheck :: HP.Pool -> BasicAuthCheck U.User
grantCheck pool = BasicAuthCheck check
  where
    check :: BasicAuthData -> IO (BasicAuthResult U.User)
    check (BasicAuthData username password) = do
        result <-
            HP.use
                pool
                (HS.query
                     (decodeUtf8 username, decodeUtf8 password)
                     U.validatePassword)
        return $
            either (const Unauthorized) (maybe Unauthorized Authorized) result

context :: HP.Pool
        -> Context (AuthHandler Request () ': AuthHandler Request U.User ': AuthHandler Request U.User ': BasicAuthCheck U.User ': '[])
context pool = captchaCheck pool :. userCheck pool :. adminCheck pool :. grantCheck pool :. EmptyContext
