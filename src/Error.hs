module Error (errorHandler) where

import           Control.Monad.Except
import           Data.String.Conv
import qualified Hasql.Pool           as HP
import           Servant.Server

errorHandler :: String -> Int -> HP.UsageError -> Handler a
errorHandler g l x = do
  let bs = toS $ g ++ ":L" ++ show l ++ " " ++ show x
  throwError (err500 { errReasonPhrase = bs })
