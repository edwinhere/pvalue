{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Entry
    (
      Entry(Entry),
      Code(DEPOSIT, WITHDRAWAL),
      _eyId,
      _eyUserId,
      _eyType,
      _eyAmount,
      _eyTradeId,
      _eyCreatedAt,
      createEntry,
      getBalance
    ) where

import           Common
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Default
import           Data.Functor.Contravariant
import           Data.Int
import           Data.Maybe
import           Data.Scientific
import           Data.Semigroup
import           Data.String.Conv
import qualified Data.Text                  as T
import           Data.Time.Clock
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import qualified Hasql.Session              as HS

data Entry = Entry {
    _eyId        :: EntryId,
    _eyUserId    :: UserId,
    _eyType      :: Code,
    _eyAmount    :: Scientific,
    _eyTradeId   :: Maybe TradeId,
    _eyCreatedAt :: UTCTime
  } deriving (Eq, Show, Generic)

data Code = DEPOSIT | WITHDRAWAL deriving (Eq, Show, Generic)

instance ToJSON Code
instance FromJSON Code

instance Default Entry where
  def = Entry def def def def def def

instance Default Code where
  def = DEPOSIT

instance ToJSON Entry where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON Entry where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

codeValueDecoder :: HD.Value Code
codeValueDecoder =
  let
    typify :: T.Text -> Maybe Code
    typify t = case T.toUpper t of
      "DEPOSIT" -> Just DEPOSIT
      "WITHDRAWAL" -> Just WITHDRAWAL
      _ -> Nothing
    in HD.enum typify

createEntry :: HQ.Query Entry Entry
createEntry =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ WITH new_entry AS ( \
      \   INSERT INTO pvalue.entries( \
      \               user_id, type, amount, trade_id) \
      \       VALUES ($1, $2::pvalue.code, $3, $4) RETURNING * \
      \ ) \
      \ SELECT id, user_id, type, amount, trade_id, created_at \
      \   FROM new_entry \
      \"
      encoder :: HE.Params Entry
      encoder =
        contramap (unUserId . _eyUserId) (HE.value HE.uuid) <>
        contramap _eyType (HE.value (HE.enum (toS . show))) <>
        contramap _eyAmount (HE.value HE.numeric) <>
        contramap (fmap unTradeId . _eyTradeId) (HE.nullableValue HE.uuid)
      decoder :: HD.Result Entry
      decoder = HD.singleRow $
        Entry <$>
          (EntryId <$> HD.value HD.uuid) <*>
          (UserId <$> HD.value HD.uuid) <*>
          HD.value codeValueDecoder <*>
          HD.value HD.numeric <*>
          HD.nullableValue (TradeId <$> HD.uuid) <*>
          HD.value HD.timestamptz

getBalance :: HQ.Query UserId Scientific
getBalance =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ SELECT \
      \   SUM( \
      \     e.amount * CASE e.type \
      \       WHEN 'DEPOSIT'::pvalue.code THEN 1 \
      \       WHEN 'WITHDRAWAL'::pvalue.code THEN -1 \
      \     END \
      \   ) AS balance \
      \ FROM pvalue.entries e \
      \ WHERE e.user_id = $1 \
      \"
      encoder :: HE.Params UserId
      encoder =
        contramap unUserId (HE.value HE.uuid)
      decoder :: HD.Result Scientific
      decoder = HD.singleRow $
        HD.value HD.numeric
