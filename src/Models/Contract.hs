
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Contract
    (
      Contract(Contract),
      _ctId,
      _ctUserId,
      _ctStatus,
      _ctTitle,
      _ctDescription,
      _ctExpiresAt,
      _ctCreatedAt,
      upsertContract
    ) where

import           Common
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Default
import           Data.Functor.Contravariant
import           Data.Semigroup
import           Data.String.Conv
import qualified Data.Text                  as T
import           Data.Time.Clock
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ

data Contract = Contract {
    _ctId          :: ContractId,
    _ctUserId      :: UserId,
    _ctStatus      :: Status,
    _ctTitle       :: T.Text,
    _ctDescription :: T.Text,
    _ctExpiresAt   :: UTCTime,
    _ctCreatedAt   :: UTCTime
  } deriving (Eq, Show, Generic)

data Status = APPROVED | REJECTED | PENDING deriving (Eq, Show, Generic)

instance ToJSON Status
instance FromJSON Status

instance Default Contract where
  def = Contract def def def def def def def

instance Default Status where
  def = PENDING

instance ToJSON Contract where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON Contract where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

statusValueDecoder :: HD.Value Status
statusValueDecoder =
  let
    typify :: T.Text -> Maybe Status
    typify t = case T.toUpper t of
      "PENDING" -> Just PENDING
      "REJECTED" -> Just REJECTED
      "APPROVED" -> Just APPROVED
      _ -> Nothing
    in HD.enum typify

upsertContract :: HQ.Query Contract Contract
upsertContract = composeUpsert' updateContract createContract

createContract :: HQ.Query Contract Contract
createContract =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ WITH new_contract AS ( \
      \   INSERT INTO  \
      \     pvalue.contracts(user_id, title, description, expires_at) \
      \   VALUES ($1, $2, $3, $4) \
      \   RETURNING * \
      \ ) \
      \\
      \ SELECT  \
      \   id,  \
      \   user_id,  \
      \   status,  \
      \   title,  \
      \   description,  \
      \   expires_at,  \
      \   created_at \
      \ FROM new_contract \
      \"
      encoder :: HE.Params Contract
      encoder =
        contramap (unUserId . _ctUserId) (HE.value HE.uuid) <>
        contramap _ctTitle (HE.value HE.text) <>
        contramap _ctDescription (HE.value HE.text) <>
        contramap _ctExpiresAt (HE.value HE.timestamptz)
      decoder :: HD.Result Contract
      decoder = HD.singleRow $
        Contract <$>
          (ContractId <$> HD.value HD.uuid) <*>
          (UserId <$> HD.value HD.uuid) <*>
          HD.value statusValueDecoder <*>
          HD.value HD.text <*>
          HD.value HD.text <*>
          HD.value HD.timestamptz <*>
          HD.value HD.timestamptz

updateContract :: HQ.Query Contract Bool
updateContract =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ UPDATE  \
      \   pvalue.contracts \
      \ SET \
      \   user_id=$1, status=$2::pvalue.status, title=$3, description=$4, expires_at=$5 \
      \ WHERE \
      \   id=$6 \
      \"
      encoder :: HE.Params Contract
      encoder =
        contramap (unUserId . _ctUserId) (HE.value HE.uuid) <>
        contramap _ctStatus (HE.value (HE.enum (toS . show))) <>
        contramap _ctTitle (HE.value HE.text) <>
        contramap _ctDescription (HE.value HE.text) <>
        contramap _ctExpiresAt (HE.value HE.timestamptz) <>
        contramap (unContractId . _ctId) (HE.value HE.uuid)
      decoder :: HD.Result Bool
      decoder = (> 0) <$> HD.rowsAffected
