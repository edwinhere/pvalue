{-# LANGUAGE Arrows            #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.User
    (
      createUser',
      User(User),
      _urId,
      findUserByUsername,
      findUserById,
      changePassword,
      validatePassword,
      findNUsersBefore
    ) where

import           Common
import           Control.Arrow
import           Data.Aeson
import           Data.Aeson.TH
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import           Data.Int
import           Data.Maybe
import           Data.Semigroup
import qualified Data.Text                  as T
import           Data.Time.Clock
import qualified Data.Vector                as V
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import           Prelude                    ()
import           Prelude.Compat

data User = User {
    _urId        :: UserId,
    _urUsername  :: T.Text,
    _urCreatedAt :: UTCTime
  } deriving (Eq, Show, Generic)

instance ToJSON User where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON User where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

createUser' :: HQ.Query (T.Text, T.Text) ()
createUser' =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "INSERT INTO pvalue.users(username, _password) VALUES ($1, crypt($2, gen_salt('bf', 8)))"
    encoder :: HE.Params (T.Text, T.Text)
    encoder =
      contramap fst (HE.value HE.text) <>
      contramap snd (HE.value HE.text)
    decoder :: HD.Result ()
    decoder = HD.unit

findUserByUsername :: HQ.Query T.Text User
findUserByUsername =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "SELECT id, username, created_at FROM pvalue.users WHERE username = $1"
    encoder :: HE.Params T.Text
    encoder = HE.value HE.text
    decoder :: HD.Result User
    decoder = HD.singleRow $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz

findUserById :: HQ.Query UserId User
findUserById =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "SELECT id, username, created_at FROM pvalue.users WHERE id = $1"
    encoder :: HE.Params UserId
    encoder = contramap unUserId (HE.value HE.uuid)
    decoder :: HD.Result User
    decoder = HD.singleRow $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz

changePassword :: HQ.Query (UserId, T.Text) Int64
changePassword =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "UPDATE pvalue.users SET _password=crypt($2, gen_salt('bf', 8)) WHERE id = $1"
    encoder :: HE.Params (UserId, T.Text)
    encoder =
      contramap (unUserId . fst) (HE.value HE.uuid) <>
      contramap snd (HE.value HE.text)
    decoder :: HD.Result Int64
    decoder = HD.rowsAffected

validatePassword :: HQ.Query (T.Text, T.Text) (Maybe User)
validatePassword =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "SELECT id, username, created_at FROM pvalue.users WHERE username=$1 AND _password=crypt($2, _password)"
    encoder :: HE.Params (T.Text, T.Text)
    encoder =
      contramap fst (HE.value HE.text) <>
      contramap snd (HE.value HE.text)
    decoder :: HD.Result (Maybe User)
    decoder = HD.maybeRow $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz

findNUsersBefore :: HQ.Query (Int64, Maybe UTCTime) (V.Vector User)
findNUsersBefore =
  proc (n, t) ->
    case t of
      Nothing -> do
        users <- findNUsersBeforeNow -< n
        returnA -< users
      Just t' -> do
        users <- findNUsersBefore' -< (n, t')
        returnA -< users

findNUsersBeforeNow :: HQ.Query Int64 (V.Vector User)
findNUsersBeforeNow =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "SELECT id, username, created_at FROM pvalue.users WHERE created_at < CURRENT_TIMESTAMP ORDER BY created_at DESC LIMIT $1"
    encoder :: HE.Params Int64
    encoder = HE.value HE.int8
    decoder :: HD.Result (V.Vector User)
    decoder = HD.rowsVector $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz

findNUsersBefore' :: HQ.Query (Int64, UTCTime) (V.Vector User)
findNUsersBefore' =
  HQ.statement sql encoder decoder True
  where
    sql :: BS.ByteString
    sql = "SELECT id, username, created_at FROM pvalue.users WHERE created_at < $2 ORDER BY created_at DESC LIMIT $1"
    encoder :: HE.Params (Int64, UTCTime)
    encoder =
      contramap fst (HE.value HE.int8) <>
      contramap snd (HE.value HE.timestamptz)
    decoder :: HD.Result (V.Vector User)
    decoder = HD.rowsVector $ User <$> (UserId <$> HD.value HD.uuid) <*> HD.value HD.text <*> HD.value HD.timestamptz
