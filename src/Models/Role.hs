{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Role
    (
      RoleType(USER,ADMIN),
      insertRole,
      findRolesByUserId,
      _rlRole
    ) where

import           Common
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import           Data.Semigroup
import           Data.String.Conv
import qualified Data.Text                  as Te
import           Data.Time.Clock
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ

data Role = Role {
  _rlId        :: RoleId,
  _rlUserId    :: UserId,
  _rlRole      :: RoleType,
  _rlCreatedAt :: UTCTime
} deriving (Eq, Show, Generic)

data RoleType = ADMIN | USER deriving (Eq, Show, Generic)

instance ToJSON RoleType
instance FromJSON RoleType

instance ToJSON Role where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON Role where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

roleValueDecoder :: HD.Value RoleType
roleValueDecoder =
  let
    typify :: Te.Text -> Maybe RoleType
    typify t = case Te.toUpper t of
      "ADMIN" -> Just ADMIN
      "USER" -> Just USER
      _ -> Nothing
    in HD.enum typify

insertRole :: HQ.Query (UserId, RoleType) ()
insertRole =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "INSERT INTO pvalue.roles(user_id, role) VALUES ($1, $2::pvalue.role)"
      encoder :: HE.Params (UserId, RoleType)
      encoder =
        contramap (unUserId . fst) (HE.value HE.uuid) <>
        contramap snd (HE.value (HE.enum (toS . show)))
      decoder :: HD.Result ()
      decoder = HD.unit

findRoleById :: HQ.Query RoleId Role
findRoleById =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "SELECT id, user_id, role, created_at FROM pvalue.roles WHERE id=$1"
      encoder :: HE.Params RoleId
      encoder = contramap unRoleId (HE.value HE.uuid)
      decoder :: HD.Result Role
      decoder = HD.singleRow $
        Role <$>
          (RoleId <$> HD.value HD.uuid) <*>
          (UserId <$> HD.value HD.uuid) <*>
          HD.value roleValueDecoder <*>
          HD.value HD.timestamptz

findRolesByUserId :: HQ.Query UserId [Role]
findRolesByUserId =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "SELECT id, user_id, role, created_at FROM pvalue.roles WHERE user_id=$1"
      encoder :: HE.Params UserId
      encoder = contramap unUserId (HE.value HE.uuid)
      decoder :: HD.Result [Role]
      decoder = HD.rowsList $
        Role <$>
          (RoleId <$> HD.value HD.uuid) <*>
          (UserId <$> HD.value HD.uuid) <*>
          HD.value roleValueDecoder <*>
          HD.value HD.timestamptz
