{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Trade
    (
      BuySell(BUY, SELL),
      Trade(Trade),
      _trId,
      _trUserId,
      _trBuySell,
      _trOutcomeId,
      _trQuantity,
      _trPrice,
      _trCreatedAt,
      createTrade,
      getHoldings
    ) where

import           Common
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Default
import           Data.Functor.Contravariant
import           Data.Int
import           Data.Maybe
import           Data.Scientific
import           Data.Semigroup
import           Data.String.Conv
import qualified Data.Text                  as T
import           Data.Time.Clock
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import qualified Hasql.Session              as HS

data Trade = Trade {
    _trId        :: TradeId,
    _trUserId    :: UserId,
    _trBuySell   :: BuySell,
    _trOutcomeId :: OutcomeId,
    _trQuantity  :: Scientific,
    _trPrice     :: Scientific,
    _trCreatedAt :: UTCTime
  } deriving (Eq, Show, Generic)

data BuySell = BUY | SELL deriving (Eq, Show, Generic)

instance ToJSON BuySell
instance FromJSON BuySell

instance Default BuySell where
  def = BUY

instance Default Trade where
  def = Trade def def def def def def def

instance ToJSON Trade where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON Trade where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

buySellDecoder :: HD.Value BuySell
buySellDecoder =
  let
    typify :: T.Text -> Maybe BuySell
    typify t = case T.toUpper t of
      "BUY" -> Just BUY
      "SELL" -> Just SELL
      _ -> Nothing
    in HD.enum typify

createTrade :: HQ.Query Trade Trade
createTrade =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ WITH new_trade AS ( \
      \   INSERT INTO pvalue.trades( \
      \               user_id, buy_sell, outcome_id, quantity, price) \
      \       VALUES ($1, $2::pvalue.buy_sell, $3, $4, $5) RETURNING * \
      \ ) \
      \ SELECT id, user_id, buy_sell, outcome_id, quantity, price, created_at \
      \   FROM new_trade \
      \"
      encoder :: HE.Params Trade
      encoder =
        contramap (unUserId . _trUserId) (HE.value HE.uuid) <>
        contramap _trBuySell (HE.value (HE.enum (toS . show))) <>
        contramap (unOutcomeId . _trOutcomeId) (HE.value HE.uuid) <>
        contramap _trQuantity (HE.value HE.numeric) <>
        contramap _trPrice (HE.value HE.numeric)
      decoder :: HD.Result Trade
      decoder = HD.singleRow $
        Trade <$>
          (TradeId <$> HD.value HD.uuid) <*>
          (UserId <$> HD.value HD.uuid) <*>
          HD.value buySellDecoder <*>
          (OutcomeId <$> HD.value HD.uuid) <*>
          HD.value HD.numeric <*>
          HD.value HD.numeric <*>
          HD.value HD.timestamptz

getHoldings :: HQ.Query (UserId, OutcomeId) Scientific
getHoldings =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ SELECT \
      \   SUM(t.quantity * CASE t.buy_sell \
      \                       WHEN 'BUY'::pvalue.buy_sell THEN 1 \
      \                       WHEN 'SELL'::pvalue.buy_sell THEN -1 \
      \                     END) AS holding \
      \ FROM pvalue.trades t \
      \ WHERE t.user_id = $1 AND \
      \ t.outcome_id = $2 \
      \"
      encoder :: HE.Params (UserId, OutcomeId)
      encoder =
        contramap (unUserId . fst) (HE.value HE.uuid) <>
        contramap (unOutcomeId . snd) (HE.value HE.uuid)
      decoder :: HD.Result Scientific
      decoder = HD.singleRow $
        HD.value HD.numeric
