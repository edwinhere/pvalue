{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Captcha (
                        Captcha(Captcha),
                        _cpId,
                        createCaptcha,
                        checkCaptcha,
                        cheatCaptcha
                      ) where

import           Common
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import           Data.Semigroup
import qualified Data.Text                  as T
import           Data.Time.Clock
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import qualified Hasql.Session              as HS

data Captcha = Captcha {
    _cpId        :: CaptchaId,
    _cpImage     :: ByteString64,
    _cpCreatedAt :: UTCTime
} deriving (Eq, Show, Generic)

instance ToJSON Captcha where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON Captcha where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

createCaptcha :: HQ.Query (T.Text, BS.ByteString) Captcha
createCaptcha =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
            \WITH NEW_CAPTCHA AS (\
                \INSERT INTO PVALUE.CAPTCHAS (SOLUTION, IMAGE) \
                \VALUES ($1, $2) \
                \RETURNING ID, IMAGE, CREATED_AT) \
            \SELECT \
                \NEW_CAPTCHA.ID, \
                \NEW_CAPTCHA.IMAGE, \
                \NEW_CAPTCHA.CREATED_AT FROM NEW_CAPTCHA"
      encoder :: HE.Params (T.Text, BS.ByteString)
      encoder = contramap fst (HE.value HE.text) <>
                contramap snd (HE.value HE.bytea)
      decoder :: HD.Result Captcha
      decoder = HD.singleRow $
        Captcha <$>
          (CaptchaId <$> HD.value HD.uuid) <*>
          (ByteString64 <$> HD.value HD.bytea) <*>
          HD.value HD.timestamptz

checkCaptcha :: HQ.Query (CaptchaId, T.Text) Bool
checkCaptcha =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "SELECT \
                \COUNT(*) \
                \FROM PVALUE.CAPTCHAS \
                \WHERE ID=$1 AND SOLUTION=$2"
      encoder :: HE.Params (CaptchaId, T.Text)
      encoder =
        contramap (unCaptchaId . fst) (HE.value HE.uuid) <>
        contramap snd (HE.value HE.text)
      decoder :: HD.Result Bool
      decoder = (>0) <$>
        HD.singleRow (HD.value HD.int8)

-- For testing purposes only
cheatCaptcha :: HQ.Query Captcha T.Text
cheatCaptcha =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "SELECT SOLUTION FROM PVALUE.CAPTCHAS WHERE ID=$1"
      encoder :: HE.Params Captcha
      encoder = contramap (unCaptchaId . _cpId) (HE.value HE.uuid)
      decoder :: HD.Result T.Text
      decoder = HD.singleRow $ HD.value HD.text
