{-# LANGUAGE OverloadedStrings #-}
module Models.UserPayment
    (
    ) where

import           Common
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import           Data.Semigroup
import           Data.String.Conv
import qualified Data.Text                  as T
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ

insertPaymentId :: HQ.Query (UserId, T.Text) ()
insertPaymentId =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "INSERT INTO pvalue.user_payment(user_id, payment_id) VALUES ($1, $2)"
      encoder :: HE.Params (UserId, T.Text)
      encoder =
        contramap (unUserId . fst) (HE.value HE.uuid) <>
        contramap snd (HE.value HE.text)
      decoder :: HD.Result ()
      decoder = HD.unit

allPaymentIds :: HQ.Query () [(UserId, T.Text)]
allPaymentIds =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = ""
      encoder :: HE.Params ()
      encoder = HE.unit
      decoder :: HD.Result [(UserId, T.Text)]
      decoder = undefined
