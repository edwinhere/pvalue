{-# LANGUAGE Arrows            #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Token
    (
      upsertToken,
      findUserIdByToken
    ) where

import           Common
import           Control.Arrow
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ

upsertToken :: HQ.Query UserId (Maybe TokenId)
upsertToken = proc u -> do
  upsertToken' -< u
  token <- findTokenByUserId -< u
  returnA -< token

upsertToken' :: HQ.Query UserId ()
upsertToken' = composeUpsert updateToken createToken

createToken :: HQ.Query UserId ()
createToken =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "INSERT INTO pvalue.tokens(user_id) VALUES ($1)"
      decoder :: HD.Result ()
      decoder = HD.unit

updateToken :: HQ.Query UserId Bool
updateToken =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "UPDATE pvalue.tokens SET token=uuid_generate_v1mc(), updated_at=CURRENT_TIMESTAMP WHERE user_id=$1"
      decoder :: HD.Result Bool
      decoder = (> 0) <$> HD.rowsAffected

findTokenByUserId :: HQ.Query UserId (Maybe TokenId)
findTokenByUserId =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "SELECT token FROM pvalue.tokens WHERE user_id=$1"
      decoder :: HD.Result (Maybe TokenId)
      decoder = HD.maybeRow (TokenId <$> HD.value HD.uuid)

encoder :: HE.Params UserId
encoder = contramap unUserId (HE.value HE.uuid)

findUserIdByToken :: HQ.Query TokenId UserId
findUserIdByToken =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "SELECT user_id FROM pvalue.tokens WHERE token=$1"
      encoder :: HE.Params TokenId
      encoder = contramap unTokenId (HE.value HE.uuid)
      decoder :: HD.Result UserId
      decoder = HD.singleRow (UserId <$> HD.value HD.uuid)
