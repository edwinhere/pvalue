{-# LANGUAGE OverloadedStrings #-}
module Models.Payments
    (
    ) where

import           Common
import qualified Data.ByteString            as BS
import           Data.Functor.Contravariant
import           Data.Semigroup
import           Data.String.Conv
import qualified Data.Text                  as T
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ
import qualified Util.Monero                as Mo

insertPayment :: HQ.Query Mo.Payment ()
insertPayment =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "INSERT INTO pvalue.payments(payment_id, tx_hash, amount, block_height, unlock_time) VALUES ($1, $2, $3, $4, $5)"
      encoder :: HE.Params Mo.Payment
      encoder =
        contramap Mo._ptPaymentId (HE.value HE.text) <>
        contramap Mo._ptTxHash (HE.value HE.text) <>
        contramap (fromIntegral . Mo._ptAmount) (HE.value HE.int8) <>
        contramap (fromIntegral . Mo._ptBlockHeight) (HE.value HE.int8) <>
        contramap (fromIntegral . Mo._ptUnlockTime) (HE.value HE.int8)
      decoder :: HD.Result ()
      decoder = HD.unit
