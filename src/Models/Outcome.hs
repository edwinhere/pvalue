
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
module Models.Outcome
    (
      Outcome(Outcome),
      _otId,
      _otContractId,
      _otTitle,
      _otDescription,
      upsertOutcome
    ) where

import           Common
import           Data.Aeson
import           Data.Aeson.Types
import qualified Data.ByteString            as BS
import           Data.Default
import           Data.Functor.Contravariant
import           Data.Semigroup
import qualified Data.Text                  as T
import           GHC.Generics
import qualified Hasql.Decoders             as HD
import qualified Hasql.Encoders             as HE
import qualified Hasql.Query                as HQ

data Outcome = Outcome {
    _otId          :: OutcomeId,
    _otContractId  :: ContractId,
    _otTitle       :: T.Text,
    _otDescription :: T.Text
  } deriving (Eq, Show, Generic)

instance Default Outcome where
  def = Outcome def def def def

instance ToJSON Outcome where
  toJSON = genericToJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

instance FromJSON Outcome where
  parseJSON = genericParseJSON defaultOptions {fieldLabelModifier = camelTo2 '_' . drop 3}

upsertOutcome :: HQ.Query Outcome Outcome
upsertOutcome = composeUpsert' updateOutcome createOutcome

createOutcome :: HQ.Query Outcome Outcome
createOutcome =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \ WITH new_outcome AS ( \
      \   INSERT INTO  \
      \     pvalue.outcomes(contract_id, title, description) \
      \   VALUES ($1, $2, $3) \
      \   RETURNING * \
      \ ) \
      \ SELECT id, contract_id, title, description FROM new_outcome \
      \"
      encoder :: HE.Params Outcome
      encoder =
        contramap (unContractId . _otContractId) (HE.value HE.uuid) <>
        contramap _otTitle (HE.value HE.text) <>
        contramap _otDescription (HE.value HE.text)
      decoder :: HD.Result Outcome
      decoder = HD.singleRow $
        Outcome <$>
          (OutcomeId <$> HD.value HD.uuid) <*>
          (ContractId <$> HD.value HD.uuid) <*>
          HD.value HD.text <*>
          HD.value HD.text

updateOutcome :: HQ.Query Outcome Bool
updateOutcome =
  HQ.statement sql encoder decoder True
    where
      sql :: BS.ByteString
      sql = "\
      \   UPDATE \
      \     pvalue.outcomes \
      \   SET \
      \     contract_id=$1, title=$2, description=$3 \
      \   WHERE id=$4 \
      \"
      encoder :: HE.Params Outcome
      encoder =
        contramap (unContractId . _otContractId) (HE.value HE.uuid) <>
        contramap _otTitle (HE.value HE.text) <>
        contramap _otDescription (HE.value HE.text) <>
        contramap (unOutcomeId . _otId) (HE.value HE.uuid)
      decoder :: HD.Result Bool
      decoder = (> 0) <$> HD.rowsAffected
