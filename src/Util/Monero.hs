{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE DeriveGeneric     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Util.Monero
    (
      getBalance,
      getAddress,
      getHeight,
      transfer,
      transferSplit,
      Destination(..),
      Payment(..),
      sweepDust,
      store,
      getPayments,
      getBulkPayments,
      incomingTransfers,
      queryKey,
      makeIntegratedAddress,
      splitIntegratedAddress,
      stopWallet
    ) where

import           Control.Monad.Trans.Except (ExceptT, runExceptT)
import           Data.Aeson
import           Data.Aeson.Types
import           Data.Default
import           Data.Proxy
import           Data.Scientific
import qualified Data.Text                  as T
import           Debug.Trace
import           GHC.Generics
import           Network.HTTP.Client        (Manager, defaultManagerSettings,
                                             newManager)
import           Servant.API
import           Servant.Client

data Destination = Destination {
    _dnAmount  :: Integer,
    _dnAddress :: T.Text
  } deriving (Show, Generic)

instance ToJSON Destination where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON Destination where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

data RPCParams = TransferP {
                    _tspDestinations  :: [Destination],
                    _tspPriority      :: Maybe Integer,
                    _tspMixin         :: Maybe Integer,
                    _tspUnlockTime    :: Maybe Integer,
                    _tspPaymentId     :: Maybe T.Text,
                    _tspGetTxKeys     :: Maybe Bool,
                    _tspTrustedDaemon :: Maybe Bool
                  }
  | TransferSplitP {
        _tspDestinations  :: [Destination],
        _tspPriority      :: Maybe Integer,
        _tspMixin         :: Maybe Integer,
        _tspUnlockTime    :: Maybe Integer,
        _tspPaymentId     :: Maybe T.Text,
        _tspGetTxKeys     :: Maybe Bool,
        _tspTrustedDaemon :: Maybe Bool
      }
  | GetPaymentsP {
        _gppPaymentId :: T.Text
      }
  | GetBulkPaymentsP {
        _gbpPaymentIds     :: [T.Text],
        _gbpMinBlockHeight :: Integer
      }
  | IncomingTransfersP {
        _itpTransferType :: T.Text
      }
  | QueryKeyP {
        _qkpKeyType :: T.Text
      }
  | MakeIntegratedAddressP {
        _mipPaymentId :: T.Text
      }
  | SplitIntegratedAddressP {
        _sipIntegratedAddress :: T.Text
      }
  deriving (Show, Generic)

instance ToJSON RPCParams where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 4,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON RPCParams where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 4,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

data RPCRequest = RPCRequest {
    _rqJsonrpc :: T.Text,
    _rqId      :: T.Text,
    _rqMethod  :: T.Text,
    _rqParams  :: Maybe RPCParams
  } deriving (Show, Generic)

instance Default RPCRequest where
  def = RPCRequest "2.0" "0" "getbalance" Nothing

instance ToJSON RPCRequest where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON RPCRequest where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

data Payment = Payment {
    _ptPaymentId   :: T.Text,
    _ptTxHash      :: T.Text,
    _ptAmount      :: Integer,
    _ptBlockHeight :: Integer,
    _ptUnlockTime  :: Integer
  } deriving (Show, Generic)

instance ToJSON Payment where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON Payment where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

data Transfer = Transfer {
    _tfAmount      :: Integer,
    _tfSpent       :: Bool,
    _tfGlobalIndex :: Integer,
    _tfTxHash      :: T.Text,
    _tfTxSize      :: Integer
  } deriving (Show, Generic)

instance ToJSON Transfer where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON Transfer where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

data RPCResult = GetBalanceR {
                    _gbrBalance         :: Integer,
                    _gbrUnlockedBalance :: Integer
                  }
  | GetAddressR {
        _garAddress :: T.Text
      }
  | GetHeightR {
        _ghrHeight :: Integer
      }
  | TransferR {
        _tfrTxHash :: T.Text,
        _tfrTxKey  :: T.Text
      }
  | TransferSplitR {
        _tsrTxHashList :: [T.Text],
        _tsrTxKeyList  :: Maybe [T.Text]
      }
  | SweepDustR {
        _sdrTxHashList :: [T.Text]
      }
  | GetPaymentsR {
        _gprPayments :: [Payment]
      }
  | IncomingTransfersR {
        _itrTransfers :: [Transfer]
      }
  | QueryKeyR {
        _qkyKey :: T.Text
      }
  | MakeIntegratedAddressR {
        _mirIntegratedAddress :: T.Text,
        _mirPaymentId         :: T.Text
      }
  | SplitIntegratedAddressR {
        _sirStandardAddress :: T.Text,
        _sirPaymentId       :: T.Text
      }
  | Blank {
      _placeholder :: Maybe T.Text
    }
  deriving (Show, Generic)

instance ToJSON RPCResult where
  toJSON = genericToJSON defaultOptions {
    fieldLabelModifier = camelTo2 '_' . drop 4,
    omitNothingFields = True,
    sumEncoding = UntaggedValue
  }

instance FromJSON RPCResult where
  parseJSON = genericParseJSON defaultOptions {
    fieldLabelModifier = camelTo2 '_' . drop 4,
    omitNothingFields = True,
    sumEncoding = UntaggedValue
  }

data RPCError = RPCError {
    _erCode    :: Integer,
    _erMessage :: T.Text
  } deriving (Show, Generic)

instance ToJSON RPCError where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON RPCError where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

data RPCResponse = RPCResponse {
    _rsError   :: Maybe RPCError,
    _rsJsonrpc :: T.Text,
    _rsId      :: T.Text,
    _rsResult  :: RPCResult
  } deriving (Show, Generic)

instance ToJSON RPCResponse where
  toJSON = genericToJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

instance FromJSON RPCResponse where
  parseJSON = genericParseJSON defaultOptions {
      fieldLabelModifier = camelTo2 '_' . drop 3,
      omitNothingFields = True,
      sumEncoding = UntaggedValue
    }

type API = "json_rpc" :> ReqBody '[JSON] RPCRequest :> Post '[JSON] RPCResponse

proxy :: Proxy API
proxy = Proxy

query :: RPCRequest -> Manager -> BaseUrl -> ExceptT ServantError IO RPCResponse
query = client proxy

baseUrl :: BaseUrl
baseUrl = BaseUrl Http "localhost" 8085 ""

getBalance :: IO (Scientific, Scientific)
getBalance = do
  manager <- newManager defaultManagerSettings
  res <- runExceptT (query def manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (GetBalanceR c u)) -> return (scientific c (-12), scientific u (-12))
    _ -> fail "Unexpected response"

getAddress :: IO T.Text
getAddress = do
  manager <- newManager defaultManagerSettings
  let
    request = def { _rqMethod = "getaddress" }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (GetAddressR a)) -> return a
    _ -> fail "Unexpected response"

getHeight :: IO Integer
getHeight = do
  manager <- newManager defaultManagerSettings
  let
    request = def { _rqMethod = "getheight" }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (GetHeightR h)) -> return h
    _ -> fail "Unexpected response"

transfer :: [Destination]
  -> Maybe Integer
  -> Maybe Integer
  -> Maybe Integer
  -> Maybe T.Text
  -> Maybe Bool
  -> Maybe Bool
  -> IO (T.Text, T.Text)
transfer destinations priority mixin unlockTime paymentId getTxKeys trustedDaemon = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "transfer",
        _rqParams = Just TransferP {
            _tspDestinations = destinations,
            _tspPriority = priority,
            _tspMixin = mixin,
            _tspUnlockTime = unlockTime,
            _tspPaymentId = paymentId,
            _tspGetTxKeys = getTxKeys,
            _tspTrustedDaemon = trustedDaemon
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (TransferR txHash txKey)) -> return (txHash, txKey)
    _ -> fail "Unexpected response"

transferSplit :: [Destination]
  -> Maybe Integer
  -> Maybe Integer
  -> Maybe Integer
  -> Maybe T.Text
  -> Maybe Bool
  -> Maybe Bool
  -> IO ([T.Text], Maybe [T.Text])
transferSplit destinations priority mixin unlockTime paymentId getTxKeys trustedDaemon = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "transfer_split",
        _rqParams = Just TransferSplitP {
            _tspDestinations = destinations,
            _tspPriority = priority,
            _tspMixin = mixin,
            _tspUnlockTime = unlockTime,
            _tspPaymentId = paymentId,
            _tspGetTxKeys = getTxKeys,
            _tspTrustedDaemon = trustedDaemon
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (TransferSplitR txHashList txKeyList)) -> return (txHashList, txKeyList)
    _ -> fail "Unexpected response"
sweepDust :: IO (Maybe [T.Text])
sweepDust = do
  manager <- newManager defaultManagerSettings
  let
    request = def { _rqMethod = "sweep_dust" }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (SweepDustR txHashList)) -> return $ Just txHashList
    Right (RPCResponse Nothing _ _ _) -> return Nothing

store :: IO ()
store = do
  manager <- newManager defaultManagerSettings
  let
    request = def { _rqMethod = "store" }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (Blank Nothing)) -> return ()
    _ -> fail "Unexpected response"

getPayments :: T.Text -> IO (Maybe [Payment])
getPayments paymentId = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "get_payments",
        _rqParams = Just GetPaymentsP {
            _gppPaymentId = paymentId
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (GetPaymentsR payments)) -> return $ Just payments
    Right (RPCResponse Nothing _ _ (Blank Nothing)) -> return Nothing
    _ -> fail "Unexpected response"

getBulkPayments :: [T.Text] -> Integer -> IO (Maybe [Payment])
getBulkPayments paymentIds minBlockHeight = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "get_bulk_payments",
        _rqParams = Just GetBulkPaymentsP {
            _gbpPaymentIds = paymentIds,
            _gbpMinBlockHeight = minBlockHeight
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (GetPaymentsR payments)) -> return $ Just payments
    Right (RPCResponse Nothing _ _ (Blank Nothing)) -> return Nothing
    _ -> fail "Unexpected response"

incomingTransfers :: T.Text -> IO (Maybe [Transfer])
incomingTransfers transfers = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "incoming_transfers",
        _rqParams = Just IncomingTransfersP {
            _itpTransferType = transfers
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (IncomingTransfersR incoming)) -> return $ Just incoming
    Right (RPCResponse Nothing _ _ (Blank Nothing)) -> return Nothing
    _ -> fail "Unexpected response"

queryKey :: T.Text -> IO T.Text
queryKey keyType = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "query_key",
        _rqParams = Just QueryKeyP {
            _qkpKeyType = keyType
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (QueryKeyR key)) -> return key
    _ -> fail "Unexpected response"

makeIntegratedAddress :: T.Text -> IO (T.Text, T.Text)
makeIntegratedAddress paymentId = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "make_integrated_address",
        _rqParams = Just MakeIntegratedAddressP {
            _mipPaymentId = paymentId
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (MakeIntegratedAddressR address paymentId)) -> return (address, paymentId)
    _ -> fail "Unexpected response"

splitIntegratedAddress :: T.Text -> IO (T.Text, T.Text)
splitIntegratedAddress integratedAddress = do
  manager <- newManager defaultManagerSettings
  let
    request = def {
        _rqMethod = "split_integrated_address",
        _rqParams = Just SplitIntegratedAddressP {
            _sipIntegratedAddress = integratedAddress
          }
      }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ (SplitIntegratedAddressR standardAddress paymentId)) -> return (standardAddress, paymentId)
    _ -> fail "Unexpected response"

stopWallet :: IO ()
stopWallet = do
  manager <- newManager defaultManagerSettings
  let
    request = def { _rqMethod = "stop_wallet" }
  res <- runExceptT (query request manager baseUrl)
  case res of
    Left err -> fail $ show err
    Right (RPCResponse (Just err) _ _ _) -> fail $ show err
    Right (RPCResponse Nothing _ _ _) -> return ()
