{-# LANGUAGE OverloadedStrings #-}
module Services.OptionSpec (spec) where

import           Common
import qualified Data.ByteString as BS
import           Data.Default
import           Data.Either
import           Data.Int
import           Data.Scientific
import           Data.UUID
import           Data.Word
import           Debug.Trace
import qualified Hasql.Pool      as HP
import qualified Hasql.Session   as HS
import qualified Models.Contract as MC
import qualified Models.Entry    as ME
import qualified Models.Outcome  as MO
import qualified Models.Trade    as MT
import qualified Models.User     as MU
import qualified Services.Option as SO
import           Test.Hspec
import           TestCommon

spec :: Spec
spec =
  around withPool $
    describe "Services.OptionSpec" $ do
      it "can create an option" $ \pool -> do
        maybeAdmin <- HP.use pool (HS.query "admin" MU.findUserByUsername)
        let
          Right admin = maybeAdmin
          contract = def { MC._ctTitle = "Test", MC._ctDescription = "Test Description", MC._ctUserId = MU._urId admin }
          outcomes = [
              def { MO._otTitle = "First Outcome", MO._otDescription = "Second Outcome" },
              def { MO._otTitle = "Lorem Outcome", MO._otDescription = "Ipsum Outcome" }
            ]
          option = (contract, outcomes)
        result <- HP.use pool (SO.upsertOption option)
        result `shouldSatisfy` isRight
        let Right (c, os) = result
        MC._ctId c `shouldNotBe` def
        (== def) . MO._otId <$> os `shouldNotContain` [True]
        (== def) . MO._otContractId <$> os `shouldNotContain` [True]
      it "can count outstandings and holdings" $ \pool -> do
        maybeAdmin <- HP.use pool (HS.query "admin" MU.findUserByUsername)
        let
          Right admin = maybeAdmin
          contract = def { MC._ctTitle = "Will this business prevail?", MC._ctDescription = "See title", MC._ctUserId = MU._urId admin }
          outcomes = [
              def { MO._otTitle = "Yes", MO._otDescription = "Yes it will" },
              def { MO._otTitle = "No", MO._otDescription = "No it won't" }
            ]
          option = (contract, outcomes)
        maybeOption <- HP.use pool (SO.upsertOption option)
        maybeOption `shouldSatisfy` isRight
        let
          Right option' = maybeOption
          outcome1 = head $ snd option'
          outcome2 = last $ snd option'
          buy1 = def { MT._trUserId = MU._urId admin, MT._trBuySell = MT.BUY, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId outcome1}
          buy2 = def { MT._trUserId = MU._urId admin, MT._trBuySell = MT.BUY, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId outcome2}
        maybeBuy1 <- HP.use pool (HS.query buy1 MT.createTrade)
        maybeBuy1 `shouldSatisfy` isRight
        maybeBuy2 <- HP.use pool (HS.query buy2 MT.createTrade)
        maybeBuy2 `shouldSatisfy` isRight
        maybeOutstanding <- HP.use pool (HS.query (MO._otId outcome1) SO.getOutstanding)
        maybeOutstanding `shouldSatisfy` isRight
        let
          Right outstanding = maybeOutstanding
          counts = fmap snd outstanding
          total = sum counts
        total `shouldBe` 2
        counts `shouldBe` [1, 1]
        let
          buy3 = def { MT._trUserId = MU._urId admin, MT._trBuySell = MT.BUY, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId outcome2}
          sell1 = def { MT._trUserId = MU._urId admin, MT._trBuySell = MT.SELL, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId outcome2}
        maybeBuy3 <- HP.use pool (HS.query buy3 MT.createTrade)
        maybeBuy3 `shouldSatisfy` isRight
        maybeSell1 <- HP.use pool (HS.query sell1 MT.createTrade)
        maybeSell1 `shouldSatisfy` isRight
        let
          Right sell1' = maybeSell1
        maybeHoldings <- HP.use pool (HS.query (MU._urId admin, MT._trOutcomeId sell1') MT.getHoldings)
        maybeHoldings `shouldSatisfy` isRight
        let
          Right holdings = maybeHoldings
        holdings `shouldBe` 1
      it "can price and cost options correctly" $ \pool -> do
        let
          indices :: [Word32]
          indices = [1,2]
          uuids :: [UUID]
          uuids = fmap (fromWords 0 0 0) indices
          outcomeIds :: [OutcomeId]
          outcomeIds = fmap OutcomeId uuids
          outstandings :: SO.Outstanding
          outstandings = zip outcomeIds [200, 250]
          outstanding1 :: SO.Outstanding
          outstanding1 = zip outcomeIds [1000, 1000]
          outstanding2 :: SO.Outstanding
          outstanding2 = zip outcomeIds [1001,1000]
          p = SO.price (head outcomeIds) outstandings
          c = SO.cost' outstanding1 outstanding2
        p `shouldSatisfy` (\x -> x >= 0.1 && x <= 0.12)
        c `shouldSatisfy` (\x -> x > 0.5 && x < 0.55)
      it "can buy & sell options" $ \pool -> do
        maybeAdmin <- HP.use pool (HS.query "admin" MU.findUserByUsername)
        let
          Right admin = maybeAdmin
          contract = def { MC._ctTitle = "Nunc accumsan libero non nulla volutpat convallis", MC._ctDescription = "Duis ultricies velit ac lorem venenatis scelerisque.", MC._ctUserId = MU._urId admin }
          outcomes = [
              def { MO._otTitle = "Ya", MO._otDescription = "Yes it will" },
              def { MO._otTitle = "Nein", MO._otDescription = "No it won't" }
            ]
          option = (contract, outcomes)
        maybeOption <- HP.use pool (SO.upsertOption option)
        maybeOption `shouldSatisfy` isRight
        let
          Right o' = maybeOption
          o1 = head $ snd o'
          o2 = last $ snd o'
          b1 = def { MT._trUserId = MU._urId admin, MT._trBuySell = MT.BUY, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId o1}
          b2 = def { MT._trUserId = MU._urId admin, MT._trBuySell = MT.BUY, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId o2}
        maybeBuy1 <- HP.use pool (HS.query b1 MT.createTrade)
        maybeBuy1 `shouldSatisfy` isRight
        maybeBuy2 <- HP.use pool (HS.query b2 MT.createTrade)
        maybeBuy2 `shouldSatisfy` isRight
        let
          deposit = def { ME._eyType = ME.DEPOSIT, ME._eyAmount = fromFloatDigits 1.0, ME._eyUserId = MU._urId admin, ME._eyTradeId = Nothing }
        maybeDeposit <- HP.use pool (HS.query deposit ME.createEntry)
        maybeDeposit `shouldSatisfy` isRight
        let
          Right option = maybeOption
        maybeTradeId <- HP.use pool (SO.buyOption (MU._urId admin) ((MO._otId . head . snd) option) 1)
        maybeTradeId `shouldSatisfy` isRight
        maybeSellId <- HP.use pool (SO.sellOption (MU._urId admin) ((MO._otId . head . snd) option) 1)
        maybeSellId `shouldSatisfy` isRight
