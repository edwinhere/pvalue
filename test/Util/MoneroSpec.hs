{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}
module Util.MoneroSpec
    (
      spec
    ) where

import           Data.Maybe
import qualified Data.Text      as T
import           Test.Hspec
import qualified Test.Hspec.Wai as HW
import qualified Util.Monero    as Mo

spec :: Spec
spec = describe "Wallet RPC" $ do
  it "can get balance" $ do
    (c, u) <- Mo.getBalance
    c `shouldSatisfy` (>= 0)
    u `shouldSatisfy` (>= 0)
  it "can get address" $ do
    a <- Mo.getAddress
    a `shouldSatisfy` (\s -> T.length s == 95)
  it "can get height" $ do
    h <- Mo.getHeight
    h `shouldSatisfy` (> 0)
  it "can sweep dust" $ do
    result <- Mo.sweepDust
    result `shouldSatisfy` (\s -> isJust s || isNothing s)
  it "can transfer money" $ do
    let
      ds = [Mo.Destination 100000000000 "A2rgGdM78JEQcxEUsi761WbnJWsFRCwh1PkiGtGnUUcJTGenfCr5WEtdoXezutmPiQMsaM4zJbpdH5PMjkCt7QrXAhV8wDB"]
    (txHash, txKey) <- Mo.transfer ds Nothing Nothing Nothing Nothing Nothing Nothing
    txHash `shouldSatisfy` (\s -> T.length s == 64)
    txKey `shouldSatisfy` T.null
  it "can split large transactions" $ do
    let
      ds = [Mo.Destination 100000000000 "A2rgGdM78JEQcxEUsi761WbnJWsFRCwh1PkiGtGnUUcJTGenfCr5WEtdoXezutmPiQMsaM4zJbpdH5PMjkCt7QrXAhV8wDB"]
    (txHashList, txKeyList) <- Mo.transferSplit ds Nothing Nothing Nothing Nothing Nothing Nothing
    txHashList `shouldSatisfy` (\s -> length s == 1)
    head txHashList `shouldSatisfy` (\s -> T.length s == 64)
    txKeyList `shouldBe` Nothing
  it "can get payments" $ do
    (Just payments) <- Mo.getPayments "95e208e4389692d7"
    payments `shouldSatisfy` \ps -> not (null ps)
    (Mo._ptTxHash . head) payments `shouldSatisfy` (\s -> T.length s == 64)
  it "can get empty payments" $ do
    payments <- Mo.getPayments "95e208e4389692d8"
    payments `shouldSatisfy` isNothing
  it "can get bulk payments" $ do
    (Just payments) <- Mo.getBulkPayments ["95e208e4389692d7"] 0
    payments `shouldSatisfy` \ps -> not (null ps)
    (Mo._ptTxHash . head) payments `shouldSatisfy` (\s -> T.length s == 64)
  it "can get empty bulk payments" $ do
    payments <- Mo.getBulkPayments ["95e208e4389692d8"] 0
    payments `shouldSatisfy` isNothing
  it "can check incoming transfers" $ do
    transfers <- Mo.incomingTransfers "available"
    transfers `shouldSatisfy` \ps -> not (null ps)
  it "can query keys" $ do
    key <- Mo.queryKey "mnemonic"
    key `shouldBe` "deftly large tirade gumball android leech sidekick opened iguana voice gels focus poaching itches network espionage much jailed vaults winter oatmeal eleven science siren winter"
  it "can make integrated address" $ do
    i <- Mo.makeIntegratedAddress "95e208e4389692d7"
    i `shouldSatisfy` (\(s, p) -> T.length s == 106 && p == "95e208e4389692d7")
    i `shouldBe` ("A7Xn9qZeVE1ZiLn66S3Qzv8QfmtcwkdXgM5cWGsXAPxoQeMQ79md51PLPCijvzk1iHbuHi91pws5B7iajTX9KTtJ6HRHb7KVPHTRM5CjKC","95e208e4389692d7")
  it "can split integrated address" $ do
    (s, p) <- Mo.splitIntegratedAddress "A7Xn9qZeVE1ZiLn66S3Qzv8QfmtcwkdXgM5cWGsXAPxoQeMQ79md51PLPCijvzk1iHbuHi91pws5B7iajTX9KTtJ6HRHb7KVPHTRM5CjKC"
    s `shouldSatisfy` (\s -> T.length s == 95)
    p `shouldSatisfy` (\s -> T.length s == 16)
  it "can store the blockchain" $
    Mo.store `shouldReturn` ()
  it "can stop wallet" $
    Mo.stopWallet `shouldReturn` ()
