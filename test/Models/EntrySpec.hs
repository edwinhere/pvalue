{-# LANGUAGE OverloadedStrings #-}
module Models.EntrySpec
    (
      spec
    ) where

import           Common
import           Data.Default
import           Data.Either
import qualified Hasql.Pool    as HP
import qualified Hasql.Session as HS
import qualified Models.Entry  as ME
import qualified Models.User   as MU
import           Test.Hspec
import           TestCommon

spec :: Spec
spec = around withPool $
  describe "Models.EntrySpec" $
    it "can create a deposit" $ \pool -> do
        maybeAdmin <- HP.use pool (HS.query "admin" MU.findUserByUsername)
        let
          Right admin = maybeAdmin
          entry = def { ME._eyUserId = MU._urId admin, ME._eyAmount = 1 }
        result <- HP.use pool (HS.query entry ME.createEntry)
        result `shouldSatisfy` isRight
