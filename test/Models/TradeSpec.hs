{-# LANGUAGE OverloadedStrings #-}
module Models.TradeSpec
    (
      spec
    ) where


import           Common
import           Data.Default
import           Data.Either
import qualified Hasql.Pool      as HP
import qualified Hasql.Session   as HS
import qualified Models.Contract as MC
import qualified Models.Outcome  as MO
import qualified Models.Trade    as MT
import qualified Models.User     as MU
import qualified Services.Option as SO
import           Test.Hspec
import           TestCommon

spec :: Spec
spec = around withPool $
  describe "Models.TradeSpec" $
    it "can create a trade" $ \pool -> do
      maybeAdmin <- HP.use pool (HS.query "admin" MU.findUserByUsername)
      let
        Right admin = maybeAdmin
        contract = def { MC._ctTitle = "Test", MC._ctDescription = "Test Description", MC._ctUserId = MU._urId admin }
        outcomes = [
            def { MO._otTitle = "First Outcome", MO._otDescription = "Second Outcome" },
            def { MO._otTitle = "Lorem Outcome", MO._otDescription = "Ipsum Outcome" }
          ]
        option = (contract, outcomes)
      maybeOption <- HP.use pool (SO.upsertOption option)
      maybeOption `shouldSatisfy` isRight
      let
        Right option' = maybeOption
        outcome = head $ snd option'
        trade = def { MT._trUserId = MU._urId admin, MT._trPrice = 1, MT._trQuantity = 1, MT._trOutcomeId = MO._otId outcome}
      result <- HP.use pool (HS.query trade MT.createTrade)
      result `shouldSatisfy` isRight
