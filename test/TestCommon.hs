{-# LANGUAGE OverloadedStrings #-}
module TestCommon
    (
      openPool,
      closePool,
      withPool
    ) where

import           Control.Exception.Base
import qualified Hasql.Connection       as HC
import qualified Hasql.Pool             as HP

settings = HC.settings host port user password database
  where
    host = "localhost"
    port = 5432
    user = "edwin"
    password = "314159"
    database = "edwin"

openPool :: IO HP.Pool
openPool = HP.acquire (20, realToFrac 3.154e+7, settings)

closePool :: HP.Pool -> IO ()
closePool = HP.release

withPool :: (HP.Pool -> IO ()) -> IO ()
withPool = bracket openPool closePool
