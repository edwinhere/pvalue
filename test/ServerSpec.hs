{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}
module ServerSpec
    (
      spec
    ) where

import           Common
import           Data.Aeson
import qualified Data.ByteString.Base64 as BSB
import qualified Data.ByteString.Char8  as BS
import           Data.CaseInsensitive   (CI (..))
import           Data.Maybe
import           Data.Proxy
import           Data.String.Conv
import qualified Data.Text              as T
import           Data.UUID
import           Debug.Trace
import           GHC.Generics
import qualified Hasql.Pool             as HP
import qualified Hasql.Session          as HS
import qualified Models.Captcha         as MC
import qualified Models.User            as MU
import           Network.HTTP.Types
import           Network.Wai.Test       (SResponse (simpleBody, simpleHeaders, simpleStatus))
import qualified Server                 as S
import           Test.Hspec
import           Test.Hspec.Wai
import           TestCommon
import           Text.Regex.TDFA        ((=~))

spec :: Spec
spec = with S.app $ do
  describe "GET /captcha" $
    it "responds with captcha" $ do
      _ <- wsCaptcha
      return ()
  describe "POST /signup" $
    it "signs up users" $ do
      captcha <- wsCaptcha
      solution <- wsSolve captcha
      let _captchaId = toText . unCaptchaId $ MC._cpId captcha
          headerValue = _captchaId `T.append` " " `T.append` solution
          header = [("Captcha", toS headerValue), ("Content-Type", "application/json")]
      r <- request methodPost "/signup" header "[\"testuser\",\"password\"]"
      let maybeUser = (decode $ simpleBody r) :: Maybe MU.User
      liftIO $ do
        simpleStatus r `shouldBe` ok200
        maybeUser `shouldNotBe` Nothing
  describe "GET /login" $
    it "logs in users" $ do
      _ <- wsLogin "testuser" "password"
      return ()
  describe "GET /admin/users" $
    it "lists users" $ do
      header <- wsLogin "admin" "qwerty"
      r <- request methodGet "/admin/users" header ""
      let maybeUsers = (decode $ simpleBody r) :: Maybe [MU.User]
      liftIO $ do
        simpleStatus r `shouldBe` ok200
        maybeUsers `shouldNotBe` Nothing

wsCaptcha :: WaiSession MC.Captcha
wsCaptcha = do
  r <- get "/captcha"
  let maybeCaptcha = (decode $ simpleBody r) :: Maybe MC.Captcha
  liftIO $ do
    simpleStatus r `shouldBe` ok200
    maybeCaptcha `shouldNotBe` Nothing
  return $ fromJust maybeCaptcha

wsSolve :: MC.Captcha -> WaiSession T.Text
wsSolve captcha = do
  solution <- liftIO $ solveCaptcha captcha
  liftIO $ solution `shouldNotBe` T.empty
  return solution

wsLogin :: BS.ByteString -> BS.ByteString -> WaiSession [Header]
wsLogin username password = do
  captcha <- wsCaptcha
  solution <- wsSolve captcha
  let _captchaId = toText . unCaptchaId $ MC._cpId captcha
      headerValue = _captchaId `T.append` " " `T.append` solution
      header = [
          ("Captcha", toS headerValue),
          basicAuth username password,
          ("Content-Type", "application/json")
        ]
  r <- request methodGet "/login" header ""
  let maybeTokenId = (decode $ simpleBody r) :: Maybe String
  liftIO $ do
    simpleStatus r `shouldBe` ok200
    maybeTokenId `shouldNotBe` Nothing
  let maybeUUID = fromString . fromJust $ maybeTokenId
  liftIO $ maybeUUID `shouldNotBe` Nothing
  let uuid = toString . fromJust $ maybeUUID
      header' :: [Header]
      header' = [
          ("Authorization", "Bearer " `BS.append` toS uuid),
          ("Content-Type", "application/json")
        ]
  return header'

solveCaptcha :: MC.Captcha -> IO T.Text
solveCaptcha captcha = do
  pool <- openPool
  _1 <- liftIO $ HP.use pool (HS.query captcha MC.cheatCaptcha)
  closePool pool
  either (const $ return T.empty) return _1

basicAuth :: BS.ByteString -> BS.ByteString -> (HeaderName, BS.ByteString)
basicAuth username password = ("Authorization", "Basic " `BS.append` encoded)
  where
    encoded :: BS.ByteString
    encoded = BSB.encode $ username `BS.append` ":" `BS.append` password
