/// <reference path="index.ts"/>
/// <reference path="./controllers/root.ts"/>
module pvalue {
    var app = getModule();
    app.config([
        "$stateProvider",
        ($stateProvider: ng.ui.IStateProvider) => {
            $stateProvider.state("root", <ng.ui.IState>{
                templateUrl: "root.html",
                controller: "RootController",
                controllerAs: "root",
                abstract: true
            });

            $stateProvider.state("root.landing", <ng.ui.IState>{
                parent: "root",
                templateUrl: "landing.html"
            });

            $stateProvider.state("root.login", <ng.ui.IState>{
                parent: "root",
                templateUrl: "login.html"
            });

            $stateProvider.state("root.register", <ng.ui.IState>{
                parent: "root",
                templateUrl: "register.html"
            });
        }
    ]);
    app.run(["$rootScope", "$state", ($rootScope: ng.IRootScopeService, $state: ng.ui.IStateService) => {
        $rootScope.$on("$stateChangeError", console.log.bind(console));
        $state.go("root.landing");
    }]);
}
