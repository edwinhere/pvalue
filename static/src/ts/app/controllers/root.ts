
/// <reference path="../index.ts"/>
module pvalue {
    "use strict";
    var app = getModule();

    class RootController {
        constructor(private $mdSidenav: ng.material.ISidenavService) {
        }

        public toggleSidebar(): void {
            this.$mdSidenav("left").toggle();
        }

        public static $inject: string[] = ["$mdSidenav"];
    }

    app.controller("RootController", RootController);
}
