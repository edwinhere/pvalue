module pvalue {
    "use strict";
    angular.module('pvalue', ['ngMaterial', 'ui.router']);

    export var getModule: () => ng.IModule = () => {
        return angular.module("pvalue");
    }
}
