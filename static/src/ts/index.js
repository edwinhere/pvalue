var pvalue;
(function (pvalue) {
    "use strict";
    angular.module('pvalue', ['ngMaterial', 'ui.router']);
    pvalue.getModule = function () {
        return angular.module("pvalue");
    };
})(pvalue || (pvalue = {}));
var pvalue;
(function (pvalue) {
    "use strict";
    var app = pvalue.getModule();
    var RootController = (function () {
        function RootController($mdSidenav) {
            this.$mdSidenav = $mdSidenav;
        }
        RootController.prototype.toggleSidebar = function () {
            this.$mdSidenav("left").toggle();
        };
        return RootController;
    }());
    RootController.$inject = ["$mdSidenav"];
    app.controller("RootController", RootController);
})(pvalue || (pvalue = {}));
var pvalue;
(function (pvalue) {
    var app = pvalue.getModule();
    app.config([
        "$stateProvider",
        function ($stateProvider) {
            $stateProvider.state("root", {
                templateUrl: "root.html",
                controller: "RootController",
                controllerAs: "root",
                abstract: true
            });
            $stateProvider.state("root.landing", {
                parent: "root",
                templateUrl: "landing.html"
            });
            $stateProvider.state("root.login", {
                parent: "root",
                templateUrl: "login.html"
            });
            $stateProvider.state("root.register", {
                parent: "root",
                templateUrl: "register.html"
            });
        }
    ]);
    app.run(["$rootScope", "$state", function ($rootScope, $state) {
            $rootScope.$on("$stateChangeError", console.log.bind(console));
            $state.go("root.landing");
        }]);
})(pvalue || (pvalue = {}));
